package de.neoskop.magnolia.hyphenation.service;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.Maps;
import de.neoskop.magnolia.hyphenation.HyphenationModule;
import info.magnolia.context.Context;
import info.magnolia.context.SystemContext;
import info.magnolia.objectfactory.Components;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Service to replace hyphens in definitions and to hyphenate texts through definitions
 *
 * @author Arne Diekmann
 * @since 01.07.15
 */
@Singleton
public class HyphenationService {
  private static final Logger LOG = LogManager.getLogger(HyphenationService.class);
  private final HyphenationModule hyphenationModule;
  private volatile Supplier<Map<String, String>> cachedDefinitions;

  @Inject
  public HyphenationService(HyphenationModule hyphenationModule) {
    this.hyphenationModule = hyphenationModule;
  }

  @PostConstruct
  public void init() {
    updateDefinitionCache(this::findAllDefinitions);
  }

  private void updateDefinitionCache(Supplier<Map<String, String>> supplier) {
    cachedDefinitions = Suppliers.memoize(supplier);
  }

  /**
   * Adds hyphens to words were definitions exist
   *
   * @param input text without hyphens
   * @return input with hyphens added
   */
  public String hyphenate(String input) {
    if (StringUtils.isBlank(input)) {
      return input;
    }

    return Arrays.stream(input.split("\\s+"))
        .map(this::processWord)
        .collect(Collectors.joining(" "));
  }

  /**
   * Reloads all definitions from the JCR repository
   *
   * @param session
   */
  public void memoizeDefinitions(Session session) {
    updateDefinitionCache(() -> findAllDefinitions(session));
  }

  /**
   * Replaces all hyphenation marks defined in {@link HyphenationService} with given replacement
   *
   * @param text input to search in
   * @param replacement replacement for hyphenation marks
   * @return text with hyphenation marks replaced
   */
  public String replaceMarks(String text, String replacement) {
    List<String> hyphenationMarks = Arrays.asList(hyphenationModule.getHyphenMarks().split(","));
    String pattern = hyphenationMarks.stream().map(Pattern::quote).collect(Collectors.joining("|"));
    return text.replaceAll(pattern, replacement);
  }

  private String processWord(String word) {
    if (word.length() < hyphenationModule.getMinWordLength()) {
      return word;
    }

    String newWord = word.toLowerCase();

    for (String search : cachedDefinitions.get().keySet()) {
      String replacement = cachedDefinitions.get().get(search);
      int position = newWord.indexOf(search.toLowerCase());

      if (position != -1) {
        String right = processWord(word.substring(position + search.length()));
        String left = processWord(word.substring(0, position));
        StringBuilder sb = new StringBuilder(left);

        if (!left.isEmpty() && isWord(left.substring(left.length() - 1, left.length()))) {
          sb.append(hyphenationModule.getHyphenReplacement());
        }

        final String finalReplacement =
            keepOriginalMajuscules(
                word.substring(position, position + search.length()), replacement);
        sb.append(replaceMarks(finalReplacement, hyphenationModule.getHyphenReplacement()));

        if (!right.isEmpty()) {
          if (isWord(right.substring(0, 1))) {
            sb.append(hyphenationModule.getHyphenReplacement());
          }

          sb.append(right);
        }

        newWord = sb.toString();
        break;
      }
    }

    return keepOriginalMajuscules(word, newWord);
  }

  private String keepOriginalMajuscules(String original, String replacement) {
    final StringBuilder originalWordSB =
        new StringBuilder(original.replaceAll("[^" + HyphenationModule.WORD_CHAR_CLASS + "]", ""));
    return replacement
        .chars()
        .mapToObj(i -> (char) i)
        .map(
            c -> {
              final String currChar = c.toString();

              if (isWord(currChar)) {
                final String originalChar = originalWordSB.substring(0, 1);
                originalWordSB.deleteCharAt(0);
                return originalChar;
              }

              return currChar;
            })
        .collect(Collectors.joining(""));
  }

  private boolean isWord(String word) {
    return word.matches("^([" + HyphenationModule.WORD_CHAR_CLASS + "]+)$");
  }

  private Map<String, String> findAllDefinitions() {
    Context context = Components.getComponent(SystemContext.class);

    try {
      Session session = context.getJCRSession(HyphenationModule.HYPHENATIONS_WORKSPACE);
      return findAllDefinitions(session);
    } catch (RepositoryException e) {
      LOG.error("Retrieving of hyphenation definitions failed!", e);
      return null;
    }
  }

  private Map<String, String> findAllDefinitions(Session session) {
    TreeMap<String, String> result =
        Maps.newTreeMap(
            Comparator.comparingInt(String::length).reversed().thenComparing(String::compareTo));

    try {
      QueryManager queryManager = session.getWorkspace().getQueryManager();
      Query query =
          queryManager.createQuery(
              "SELECT * FROM [" + HyphenationModule.HYPHENATION_NODETYPE + "] AS node", "JCR-SQL2");
      NodeIterator iterator = query.execute().getNodes();

      while (iterator.hasNext()) {
        Node node = iterator.nextNode();
        String definition = node.getProperty("definition").getString();
        String name = replaceMarks(definition, "").toLowerCase();
        result.put(name, definition);
      }
    } catch (RepositoryException e) {
      LOG.error("Retrieving of hyphenation definitions failed!", e);
    }

    return result;
  }
}
