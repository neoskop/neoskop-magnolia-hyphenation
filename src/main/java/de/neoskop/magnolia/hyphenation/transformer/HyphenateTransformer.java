package de.neoskop.magnolia.hyphenation.transformer;

import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.util.ObjectProperty;
import de.neoskop.magnolia.hyphenation.HyphenationModule;
import de.neoskop.magnolia.hyphenation.service.HyphenationService;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.transformer.basic.BasicTransformer;
import javax.jcr.RepositoryException;

/**
 * @author Arne Diekmann
 * @since 01.07.15
 */
public class HyphenateTransformer extends BasicTransformer<String> {
  public HyphenateTransformer(
      Item relatedFormItem,
      ConfiguredFieldDefinition definition,
      Class<String> type,
      I18NAuthoringSupport i18NAuthoringSupport) {
    super(relatedFormItem, definition, type, i18NAuthoringSupport);
  }

  @Override
  public void writeToItem(String newValue) {
    try {
      super.writeToItem(hyphenate(newValue));
    } catch (RepositoryException e) {
      e.printStackTrace();
    }

    Property<String> p = getOrCreateOriginalProperty();
    p.setValue(newValue);
  }

  @Override
  public String readFromItem() {
    Property<String> p = getOrCreateOriginalProperty();

    if (definition.isReadOnly()) {
      p.setReadOnly(true);
    }

    return p.getValue();
  }

  private String hyphenate(String newValue) throws RepositoryException {
    HyphenationService service =
        Components.getComponentProvider().getComponent(HyphenationService.class);
    return service.hyphenate(newValue);
  }

  private Property<String> getOrCreateOriginalProperty() {
    String propertyName = definePropertyName() + HyphenationModule.SUFFIX;
    Property<String> property = relatedFormItem.getItemProperty(propertyName);

    if (property == null) {
      Property itemProperty = relatedFormItem.getItemProperty(definePropertyName());
      String newValue = null;

      if (itemProperty != null) {
        newValue = (String) itemProperty.getValue();
      }

      property = new ObjectProperty<>(newValue, String.class);
      relatedFormItem.addItemProperty(propertyName, property);
    }

    return property;
  }
}
