package de.neoskop.magnolia.hyphenation;

import com.google.common.collect.Lists;
import java.util.List;

/**
 * This class is optional and represents the configuration for the hyphenation module. By exposing
 * simple getter/setter/adder methods, this bean can be configured via content2bean using the
 * properties and node from <tt>config:/modules/hyphenation</tt>. If you don't need this, simply
 * remove the reference to this class in the module descriptor xml.
 */
public class HyphenationModule {
  public static final String HYPHENATIONS_WORKSPACE = "hyphenations";
  public static final String HYPHENATION_NODETYPE = "mgnl:hyphenation";
  public static final String WORD_CHAR_CLASS = "\\w\\u00C0-\\u024f";
  public static final String SUFFIX = "_original";

  private String hyphenMarks = "-,|";
  private String hyphenReplacement = "\u00AD";
  private int minWordLength = 5;
  private List<String> watchedWorkspaces = Lists.newArrayList("website");

  public String getHyphenMarks() {
    return hyphenMarks;
  }

  public void setHyphenMarks(String hyphenMarks) {
    this.hyphenMarks = hyphenMarks;
  }

  public String getHyphenReplacement() {
    return hyphenReplacement;
  }

  public void setHyphenReplacement(String hyphenReplacement) {
    this.hyphenReplacement = hyphenReplacement;
  }

  public int getMinWordLength() {
    return minWordLength;
  }

  public void setMinWordLength(int minWordLength) {
    this.minWordLength = minWordLength;
  }

  public List<String> getWatchedWorkspaces() {
    return watchedWorkspaces;
  }

  public void setWatchedWorkspaces(List<String> watchedWorkspaces) {
    this.watchedWorkspaces = watchedWorkspaces;
  }
}
