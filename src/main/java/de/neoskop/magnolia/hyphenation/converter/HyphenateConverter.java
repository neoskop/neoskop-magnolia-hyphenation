package de.neoskop.magnolia.hyphenation.converter;

import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;
import de.neoskop.magnolia.hyphenation.HyphenationModule;
import de.neoskop.magnolia.hyphenation.service.HyphenationService;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.datasource.jcr.JcrDatasource;
import info.magnolia.ui.editor.converter.AbstractJcrConverter;
import info.magnolia.ui.field.ConfiguredFieldDefinition;
import java.util.Optional;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HyphenateConverter extends AbstractJcrConverter<String> {

  private final Logger log = LoggerFactory.getLogger(HyphenateConverter.class);

  public info.magnolia.ui.ValueContext<Node> valueContext;
  public ConfiguredFieldDefinition<String> fieldDefinition;

  @Inject
  public HyphenateConverter(
      JcrDatasource datasource,
      ConfiguredFieldDefinition<String> fieldDefinition,
      info.magnolia.ui.ValueContext<Node> valueContext) {
    super(datasource);
    this.valueContext = valueContext;
    this.fieldDefinition = fieldDefinition;
  }

  @Override
  public Result<String> convertToModel(String newValue, ValueContext context) {
    try {
      Property p = getOrCreateOriginalProperty();
      p.setValue(newValue);
    } catch (RepositoryException e) {
      e.printStackTrace();
    }

    try {
      return Result.ok(hyphenate(newValue));
    } catch (RepositoryException e) {
      e.printStackTrace();
    }
    return Result.error("Ein Fehler im Hyphenate ist aufgetreten.");
  }

  @Override
  public String convertToPresentation(String uuid, ValueContext context) {
    try {
      Property p = getOrCreateOriginalProperty();
      return p.getValue().getString();
    } catch (IllegalStateException | RepositoryException e) {
      e.printStackTrace();
    }
    return null;
  }

  private String hyphenate(String newValue) throws RepositoryException {
    HyphenationService service =
        Components.getComponentProvider().getComponent(HyphenationService.class);
    return service.hyphenate(newValue);
  }

  private Property getOrCreateOriginalProperty() throws RepositoryException {
    String propertyNameOriginal = fieldDefinition.getName() + HyphenationModule.SUFFIX;
    Node currentNode = null;
    Optional<Node> currentNodeOptional = valueContext.getSingle();
    if (currentNodeOptional.isPresent()) {
      currentNode = currentNodeOptional.get();
    }
    Property propertyOriginal = null;
    if (currentNode.hasProperty(propertyNameOriginal)) {
      propertyOriginal = currentNode.getProperty(propertyNameOriginal);
    }

    if (propertyOriginal == null) {
      Property currentProperty = null;
      if (currentNode.hasProperty(fieldDefinition.getName())) {
        currentProperty = currentNode.getProperty(fieldDefinition.getName());
      }
      String newOriginalValue = "";

      if (currentProperty != null) {
        newOriginalValue = currentProperty.getValue().getString();
      }
      propertyOriginal = currentNode.setProperty(propertyNameOriginal, newOriginalValue);
    }

    return propertyOriginal;
  }
}
