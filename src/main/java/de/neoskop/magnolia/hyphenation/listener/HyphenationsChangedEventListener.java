package de.neoskop.magnolia.hyphenation.listener;

import com.google.common.collect.Lists;
import de.neoskop.magnolia.hyphenation.HyphenationModule;
import de.neoskop.magnolia.hyphenation.service.HyphenationService;
import info.magnolia.cms.util.ObservationUtil;
import info.magnolia.context.MgnlContext;
import info.magnolia.context.SystemContext;
import info.magnolia.module.observation.ObservationConfiguration;
import info.magnolia.module.observation.ObservationModule;
import info.magnolia.objectfactory.Components;
import info.magnolia.repository.RepositoryConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Arne Diekmann
 * @since 03.07.15
 */
public class HyphenationsChangedEventListener implements EventListener {
  private static final Logger LOG = LoggerFactory.getLogger(HyphenationsChangedEventListener.class);
  private static ThreadLocal<Map<String, Session>> sessions =
      ThreadLocal.withInitial(
          () -> {
            final SystemContext systemContext = Components.getComponent(SystemContext.class);
            final HyphenationModule module = Components.getComponent(HyphenationModule.class);
            final List<String> workspaces = Lists.newArrayList(module.getWatchedWorkspaces());
            workspaces.add(HyphenationModule.HYPHENATIONS_WORKSPACE);
            final Map<String, Session> result = new HashMap<>();

            workspaces.forEach(
                w -> {
                  try {
                    Session session = systemContext.getJCRSession(w);
                    result.put(w, session);
                  } catch (RepositoryException e) {
                    throw new IllegalStateException(
                        "Could not retrieve session from system context for workspace " + w);
                  }
                });

            return result;
          });
  private final ReentrantLock lock = new ReentrantLock();

  @Override
  public void onEvent(EventIterator events) {
    boolean hyphenationChanged = false;

    while (events.hasNext()) {
      Event event = events.nextEvent();
      try {
        if (!event.getPath().startsWith("/jcr:")) {
          hyphenationChanged = true;
        }
      } catch (RepositoryException e) {
        LOG.error("Could not get event path", e);
      }
    }

    if (!hyphenationChanged) {
      return;
    }

    new Thread(
            () -> {
              lock.lock();
              List<ObservationConfiguration> listeners = disableEventListeners();

              final SystemContext systemContext = Components.getComponent(SystemContext.class);
              final HyphenationModule hyphenationModule =
                  Components.getComponent(HyphenationModule.class);
              MgnlContext.setInstance(systemContext);

              try {
                HyphenationService service = Components.getComponent(HyphenationService.class);
                invalidateDefinitionCache(
                    sessions.get().get(HyphenationModule.HYPHENATIONS_WORKSPACE), service);
                hyphenationModule
                    .getWatchedWorkspaces()
                    .forEach(w -> updateExistingProperties(service, w));
              } finally {
                sessions.get().forEach((w, s) -> s.logout());
                sessions.remove();
                addEventListeners(listeners);
                lock.unlock();
              }
            })
        .start();
  }

  private void addEventListeners(List<ObservationConfiguration> listeners) {
    ObservationModule module = Components.getComponent(ObservationModule.class);
    listeners.forEach(module::addListenerConfiguration);
  }

  private List<ObservationConfiguration> disableEventListeners() {
    ObservationModule module = Components.getComponent(ObservationModule.class);
    List<ObservationConfiguration> listenerConfigurations =
        ((List<ObservationConfiguration>) module.getListenerConfigurations())
            .stream()
            .filter(obs -> obs.getRepository().equals(RepositoryConstants.WEBSITE))
            .collect(Collectors.toList());

    ObservationUtil.dispose(RepositoryConstants.WEBSITE);
    return listenerConfigurations;
  }

  private void invalidateDefinitionCache(Session session, HyphenationService service) {
    service.memoizeDefinitions(session);
  }

  private void updateExistingProperties(HyphenationService service, String workspace) {
    try {
      final QueryManager queryManager =
          sessions.get().get(workspace).getWorkspace().getQueryManager();
      final Query query = queryManager.createQuery("SELECT * FROM [nt:base]", "JCR-SQL2");
      final NodeIterator nodeIt = query.execute().getNodes();

      while (nodeIt.hasNext()) {
        final Node node = nodeIt.nextNode();
        final PropertyIterator propertyIt = node.getProperties("*" + HyphenationModule.SUFFIX);

        while (propertyIt.hasNext()) {
          Property property = propertyIt.nextProperty();
          String hyphenatedName = property.getName().replace(HyphenationModule.SUFFIX, "");
          Property hyphenatedProperty = node.getProperty(hyphenatedName);
          hyphenatedProperty.setValue(service.hyphenate(property.getString()));
          LOG.debug("Updated " + hyphenatedProperty);
        }

        node.getSession().save();
      }
    } catch (RepositoryException e) {
      LOG.error("Updating of nodes failed", e);
    }
  }
}
