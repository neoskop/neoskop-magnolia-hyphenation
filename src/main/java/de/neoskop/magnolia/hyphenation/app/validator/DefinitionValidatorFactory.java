package de.neoskop.magnolia.hyphenation.app.validator;

import com.vaadin.v7.data.Validator;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

/**
 * @author Arne Diekmann
 * @since 12.08.15
 */
public class DefinitionValidatorFactory
    extends AbstractFieldValidatorFactory<DefinitionValidatorDefinition> {
  public DefinitionValidatorFactory(DefinitionValidatorDefinition definition) {
    super(definition);
  }

  @Override
  public Validator createValidator() {
    return new DefinitionValidator(getI18nErrorMessage());
  }
}
