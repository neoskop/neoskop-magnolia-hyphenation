package de.neoskop.magnolia.hyphenation.app.action;

import com.vaadin.v7.data.Item;
import de.neoskop.magnolia.hyphenation.service.HyphenationService;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.dialog.action.SaveDialogAction;
import info.magnolia.ui.dialog.action.SaveDialogActionDefinition;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * @author Arne Diekmann
 * @since 01.07.15
 */
public class SaveHyphenationAction<T extends SaveDialogActionDefinition>
    extends SaveDialogAction<T> {
  public SaveHyphenationAction(
      T definition, Item item, EditorValidator validator, EditorCallback callback) {
    super(definition, item, validator, callback);
  }

  @Override
  protected void setNodeName(Node node, JcrNodeAdapter item) throws RepositoryException {
    String newNodeName =
        Components.getComponent(NodeNameHelper.class)
            .getValidatedName(definitionWithoutHyphenationMarks(node));
    item.setNodeName(newNodeName);
    node.setProperty("search", definitionWithoutHyphenationMarks(node));
    NodeUtil.renameNode(node, newNodeName);
  }

  private String definitionWithoutHyphenationMarks(Node node) throws RepositoryException {
    String definition = node.getProperty("definition").getString();
    final HyphenationService service = Components.getComponent(HyphenationService.class);
    return service.replaceMarks(definition, "");
  }
}
