package de.neoskop.magnolia.hyphenation.app.column;

import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.ui.Table;
import de.neoskop.magnolia.hyphenation.service.HyphenationService;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;
import info.magnolia.ui.workbench.column.definition.AbstractColumnDefinition;
import javax.inject.Inject;

/**
 * @author Arne Diekmann
 * @since 01.07.15
 */
public class DefinitionColumnFormatter extends AbstractColumnFormatter<AbstractColumnDefinition> {
  private final HyphenationService service;

  @Inject
  public DefinitionColumnFormatter(
      AbstractColumnDefinition definition, HyphenationService service) {
    super(definition);
    this.service = service;
  }

  @Override
  public Object generateCell(Table source, Object itemId, Object columnId) {
    Item item = source.getItem(itemId);
    Property prop = (item == null) ? null : item.getItemProperty(columnId);

    if (prop != null && prop.getValue() != null) {
      String definition = (String) prop.getValue();
      return service.replaceMarks(definition, "<span class=\"interpunct\">·</span>");
    }

    return null;
  }
}
