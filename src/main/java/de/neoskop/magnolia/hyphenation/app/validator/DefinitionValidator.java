package de.neoskop.magnolia.hyphenation.app.validator;

import com.vaadin.v7.data.validator.AbstractStringValidator;
import de.neoskop.magnolia.hyphenation.HyphenationModule;
import info.magnolia.objectfactory.Components;
import java.util.regex.Pattern;

/**
 * @author Arne Diekmann
 * @since 12.08.15
 */
public class DefinitionValidator extends AbstractStringValidator {
  private final Pattern pattern;

  public DefinitionValidator(String i18nErrorMessage) {
    super(i18nErrorMessage);
    final HyphenationModule module = Components.getComponent(HyphenationModule.class);
    final String hyphenMarksPattern = Pattern.quote(module.getHyphenMarks().replace(",", ""));
    pattern =
        Pattern.compile("^([" + hyphenMarksPattern + HyphenationModule.WORD_CHAR_CLASS + "]+)$");
  }

  @Override
  protected boolean isValidValue(String value) {
    return value == null || value.isEmpty() || pattern.matcher(value).matches();
  }
}
