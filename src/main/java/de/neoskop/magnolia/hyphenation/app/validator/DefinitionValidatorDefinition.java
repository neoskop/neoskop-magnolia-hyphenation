package de.neoskop.magnolia.hyphenation.app.validator;

import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * @author Arne Diekmann
 * @since 12.08.15
 */
public class DefinitionValidatorDefinition extends ConfiguredFieldValidatorDefinition {
  public DefinitionValidatorDefinition() {
    setFactoryClass(DefinitionValidatorFactory.class);
  }
}
