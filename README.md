# README

Mit dem Magnolia-Modul **Hyphenation** können in einer Content-App Silbentrennungen für Wörter definiert und in Feld-Definition mit der Transformer-Klasse `de.neoskop.magnolia.hyphenation.transformer.HyphenateTransformer` beim Speichern entsprechend soft hyphens eingefügt werden.

## Abhängigkeiten

Bis Version 1.1.8:

- [Magnolia CMS][1] >= 5.3.7
  - [Observation-Modul][4]
  - [Mail-Modul][5]

Ab Version 1.2.0:

- [Magnolia CMS][1] >= 5.6.7
  - [Observation-Modul][4]
  - [Mail-Modul][5]

Ab Version 2.0.0:

- [Magnolia CMS][1] >= 6.2.11
  - [Observation-Modul][4]
  - [Mail-Modul][5]

## Installation

Als Repository muss [JitPack](https://jitpack.io/) in der `pom.xml` des Magnolia-Projektes hinzugefügt werden:

```xml
<repositories>
	<repository>
		<id>jitpack.io</id>
		<url>https://jitpack.io</url>
	</repository>
</repositories>
```

Das Modul muss dann in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-hyphenation</artifactId>
	<version>2.0.1</version>
</dependency>
```

## Konfiguration

Es können in `/modules/hyphenation/config` die Optionen konfiguriert werden:

| Name                | Typ       | Default    | Beschreibung                                                                                             |
| ------------------- | --------- | ---------- | -------------------------------------------------------------------------------------------------------- |
| `hyphenMarks`       | `String`  | `"-,⎮"`    | Komma-getrennte Liste der Zeichen, mit denen Silbentrennungen in der Content-App angegeben werden können |
| `hyphenReplacement` | `String`  | `"\u00AD"` | Zeichen mit denen erkannte Silbentrennungen ersetzt werden soll                                          |
| `minWordLength`     | `Integer` | `5`        | Mindestlänge von Worten, die auf Silbentrennungen geprüft werden                                         |

[1]: https://www.magnolia-cms.com
[2]: http://maven.neoskop.io
[3]: http://maven.apache.org/maven-release/maven-release-plugin/
[4]: https://documentation.magnolia-cms.com/display/DOCS/Observation+module
[5]: https://documentation.magnolia-cms.com/display/DOCS/Mail+module
